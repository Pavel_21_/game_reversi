package com.ads.yetigame.desktop;


import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
public static void main (String[] arg) {
		System.setProperty("user.name","EnglishWords");
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Reversi";
		config.width = 640;
		config.height = 480;
		new LwjglApplication(YetiGame.getInstance(), config);
		}
		}
