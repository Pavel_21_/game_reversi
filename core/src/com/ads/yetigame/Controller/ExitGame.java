package com.ads.yetigame.Controller;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by 12k1103 on 23.04.2016.
 */
public class ExitGame extends ClickListener {
    public void clicked(InputEvent event, float x, float y) {
        Gdx.app.exit();
    }
}
