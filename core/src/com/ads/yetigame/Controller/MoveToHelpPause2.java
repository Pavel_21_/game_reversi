package com.ads.yetigame.Controller;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by rusme_000 on 27.02.2016.
 */
public class MoveToHelpPause2 extends ClickListener {
    public void clicked(InputEvent event,float x,float y){
        YetiGame.getInstance().moveToHelpPause2();

    }
}
