package com.ads.yetigame.View;

import com.ads.yetigame.Model.ButtonState;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import static com.ads.yetigame.Model.ButtonState.*;

/**
 * Created by 12k1103 on 16.04.2016.
 */
public class Button extends Actor {
    ButtonState state;
    TextureRegion imgBlack;
    TextureRegion imgRed;
    TextureRegion current;
    public Button(TextureRegion imgBlack, TextureRegion imgRed, float x, float y)
    {
        this.imgBlack = imgBlack;
        this.imgRed = imgRed;
        setPosition(x, y);
        setSize(imgBlack.getRegionWidth(), imgBlack.getRegionHeight());
        setState(NONE);

    }

    public void setState(ButtonState state) {
        this.state = state;
        switch (state){
            case BLACK:
                current = imgBlack;
                break;
            case RED:
                current = imgRed;
                break;
            case NONE:
                current = null;
                break;
        }
    }
    public void draw(Batch batch, float parentAlpha){
        if (current != null)
            batch.draw(current, getX(), getY());

    }
}
