package com.ads.yetigame.View;

import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by rusme_000 on 13.02.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;
    ShapeRenderer shapeRenderer;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * YetiGame.getInstance().getPpuX(), y * YetiGame.getInstance().getPpuY());
        setSize(width * YetiGame.getInstance().getPpuX(), height * YetiGame.getInstance().getPpuY());
        shapeRenderer = YetiGame.getInstance().getShapeRenderer();
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }
    public ImageActor(TextureRegion img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());

        if(YetiGame.DRAWRECTS) {
            batch.end();
            if (shapeRenderer.getProjectionMatrix() == null) {
                shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            }
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.rect(getX() - 1, getY() - 1, getWidth() + 2, getHeight() + 2);
            shapeRenderer.end();
            batch.begin();
        }
    }
}