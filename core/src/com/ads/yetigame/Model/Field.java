package com.ads.yetigame.Model;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by 12k1103 on 16.04.2016.
 */
public class Field{
    private int currentPlayer;
    private int[][] move;
    public Field() {
        move = new int[10][10];
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++)
                move[i][j] = 0;
        move[4][5] = 1;
        move[4][4] = 2;
        move[5][4] = 1;
        move[5][5] = 2;
        currentPlayer = 1;
    }

    private void up(int player, int y, int x) {
        for (int j = y + 1; j < 9; j++) {
            if (move[j][x] == 0 || move[j][x] == player) {
                break;
            } else if (move[j][x] != player) {
                j = j + 1;
            }
            while (j < 9) {
                if (move[j][x] == player) {
                    int tempJ = j;
                    int tempY = y + 1;
                    while (tempY <= tempJ)
                        move[y][x] = player;
                } else {
                    j++;
                }
            }
        }
    }
    /*
         for (int j = y + 1; j < 9; j++) {
            if (move[j][x] == 0 || move[j][x]== player) {
                break;
            }else if (move[j][x] != player) {
                j = j + 1;
            }
            if (j > y + 1 && move[j][x] == player) {
                for (int tempY = y; tempY < j; tempY++) {
                    if(move[tempY][x]==0)
                        break;
                    move[tempY][x] = player;
                }
            }
        }
    }

*/





    private void down(int player, int y, int x) {
        for (int j = y - 1; j >= 0; j--) {
            if (move[j][x] == 0) {
                break;
            } else if (move[j][x] != player) {
                j = j - 1;
            }
            if ((j < y - 1) && (move[j][x] == player)) {
                for (int tempY = y; tempY > j; tempY--) {
                    if(move[tempY][x]==0)
                        break;
                    move[tempY][x] = player;
                }
            }
        }
    }

    private void right(int player, int y, int x) {
        for (int i = x + 1; i < 9; i++) {
            if (move[y][i] == 0) {
                break;
            } else if (move[y][i] != player) {
                i = i + 1;
            }
            if ((i > x + 1) && (move[y][i] == player)) {
                for (int tempX = x; tempX < i; tempX++) {
                    if(move[y][tempX]==0)
                        break;
                    move[y][tempX] = player;
                }
            }
        }
    }

    private void left(int player, int y, int x) {
        for (int i = x - 1; i >= 0; i--) {
            if (move[y][i] == 0) {
                break;
            } else if (move[y][i] != player) {
                i = i - 1;
            }
            if (i < x - 1 && move[y][i] == player ) {
                for (int tempX = x; tempX > i; tempX--) {
                    if(move[y][tempX]==0)
                        break;
                    move[y][tempX] = player;
                }
            }
        }
    }

    private boolean canPlace(int y, int x) {
        if (move[y][x] != 0)
            return false;
        if (move[y + 1][x] != 0 || move[y + 1][x + 1] != 0 || move[y][x + 1] != 0
                || move[y - 1][x + 1] != 0 || move[y - 1][x - 1] != 0
                || move[y][x - 1] != 0 || move[y - 1][x] != 0 || move[y + 1][x - 1] != 0)
            return true;

        return false;

    }

    public boolean placeAt(int x, int y) {
        if (x < 1 || y < 1 || x > 8 || y > 8 || !canPlace(y, x)) {
            return false;
        }
        move[y][x] = currentPlayer;

        //right(currentPlayer, y, x);
        up(currentPlayer, y, x);
        //left(currentPlayer, y, x);
        //down(currentPlayer, y, x);

        currentPlayer = (currentPlayer == 1 ? 2 : 1);
        return true;
    }

    public void printField() {
        System.out.println("\t  | 1 2 3 4 5 6 7 8");
        for (int i = 1; i < 9; i++) {
            System.out.format("\t %d| ", i);
            for (int j = 1; j < 9; j++)
                System.out.print(move[i][j] + " ");
            System.out.print("\n");
        }
    }

    public int getCell(int x, int y){
        return (move[y][x]);
    }
}