package com.ads.yetigame;



import com.ads.yetigame.Screens.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.HashMap;

public class YetiGame extends Game {
	public static final float WORLD_WIDTH = 640f;
	public static final float WORLD_HEIGHT = 480f;
	public static final boolean DRAWRECTS = true;
	private static YetiGame ourInstance = new YetiGame();
	public static YetiGame getInstance() {return ourInstance;}
	private YetiGame() {}

	private float ppuX;
	private float ppuY;
	private HashMap<String, TextureRegion> textureRegions;

	private SpriteBatch batch;
	private ShapeRenderer shape;
	private GameMenuScreen gameMenuScreen;
	private SettingScreen settingsScreen;
	private HelpScreen helpScreen;
	private HelpScreen2 helpScreen2;
	private ExitScreen exitScreen;
	private GameScreen gameScreen;
	private PauseScreen pauseScreen;
	private SettingsPauseScreen settingsPauseScreen;
	private HelpPauseScreen helpPauseScreen;
	private HelpPauseScreen2 helpPauseScreen2;


	public ShapeRenderer getShapeRenderer() {
		return shape;
	}

	@Override
	public void create() {
		loadGraphics();
		ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
		ppuY = Gdx.graphics.getHeight()/WORLD_HEIGHT;
		batch = new SpriteBatch();
		shape = new ShapeRenderer();
		loadGraphics();
		gameMenuScreen = new GameMenuScreen(batch);
		settingsScreen = new SettingScreen(batch);
		exitScreen = new ExitScreen(batch);
		helpScreen = new HelpScreen(batch);
		helpScreen2 = new HelpScreen2(batch);
		gameScreen = new GameScreen(batch, textureRegions);
		pauseScreen = new PauseScreen(batch);
		settingsPauseScreen = new SettingsPauseScreen(batch);
		helpPauseScreen = new HelpPauseScreen(batch);
		helpPauseScreen2 = new HelpPauseScreen2(batch);

		moveToGameMenu();
	}

	private void loadGraphics() {
		textureRegions = new HashMap<String, TextureRegion>();
		textureRegions.put("Game Background", new TextureRegion(new Texture("android/assets/game_FON.png")));
		textureRegions.put("RedChip", new TextureRegion(new Texture("android/assets/Red_chips.png")));
		textureRegions.put("BlackChip", new TextureRegion(new Texture("android/assets/Black_chips.png")));

	}

	public void moveToHelpMenu(){
		setScreen(helpScreen);
	}
	public void moveToHelpMenu2(){
		setScreen(helpScreen2);
	}
	public void moveToGameMenu(){
		setScreen(gameMenuScreen);
	}
	public void moveToSettings(){
		setScreen(settingsScreen);
	}
	public void moveToExit(){
		setScreen(exitScreen);
	}
	public void moveToGame(){setScreen(gameScreen);}
	public void moveToPauseMenu(){setScreen(pauseScreen);}
	public void moveToSettingPause(){setScreen(settingsPauseScreen);}
	public void moveToHelpPause(){setScreen(helpPauseScreen);}
	public void moveToHelpPause2(){setScreen(helpPauseScreen2);}




	public float getPpuX() {
		return ppuX;
	}
	public void setPpuX(float ppuX) {
		this.ppuX = ppuX;
	}
	public float getPpuY() {
		return ppuY;
	}
	public void setPpuY(float ppuY) {
		this.ppuY = ppuY;
	}


	public void resetGameScreen() {
		gameScreen = new GameScreen(batch, textureRegions);

	}
}
