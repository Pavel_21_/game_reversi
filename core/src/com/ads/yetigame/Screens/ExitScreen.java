package com.ads.yetigame.Screens;


import com.ads.yetigame.Controller.ExitGame;
import com.ads.yetigame.Controller.MoveToGame;
import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by rusme_000 on 06.02.2016.
 */
public class ExitScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor yesButton;
    private ImageActor noButton;


    public ExitScreen (SpriteBatch batch){
        backGround = new ImageActor(new Texture("android/assets/Exit_menu.png"), 0, 0);
        yesButton = new ImageActor(new Texture("android/assets/yes_btn.png"), 100, 75);
        yesButton.addListener(new ExitGame());
        noButton = new ImageActor(new Texture("android/assets/no_btn.png"), 340, 75);
        noButton.addListener(new MoveToGame());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(yesButton);
        stage.addActor(noButton);
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}