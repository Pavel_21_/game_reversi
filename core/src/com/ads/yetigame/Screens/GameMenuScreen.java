package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.MoveToExit;
import com.ads.yetigame.Controller.MoveToGamePlay;
import com.ads.yetigame.Controller.MoveToHelpMenu;
import com.ads.yetigame.Controller.MoveToSettings;
import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;




/**
 * Created by 12k1103 on 23.01.2016.
 */
public class GameMenuScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor helpButton;
    private ImageActor settingsButton;
    private ImageActor exitButton;


    public GameMenuScreen(SpriteBatch batch) {
        backGround = new ImageActor(new Texture("android/assets/main_menu0.png"), 0, 0);
        playButton = new ImageActor(new Texture("android/assets/play_btn.png"), 20, 150);
        playButton.addListener(new MoveToGamePlay());
        settingsButton = new ImageActor(new Texture("android/assets/setting_btn.png"), 320, 150);
        settingsButton.addListener(new MoveToSettings());
        exitButton = new ImageActor(new Texture("android/assets/Exit_btn.png"), 320, 50);
        exitButton.addListener(new MoveToExit());
        helpButton = new ImageActor(new Texture("android/assets/Help_btn.png"), 20, 50);
        helpButton.addListener(new MoveToHelpMenu());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(helpButton);
        stage.addActor(exitButton);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
    }

// 1011111=64+0+16+8+4+2+1=95;
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}