package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.MoveToGame;
import com.ads.yetigame.Controller.MoveToGamePlay;
import com.ads.yetigame.Controller.MoveToHelpPause;
import com.ads.yetigame.Controller.MoveToSettingPause;
import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by rusme_000 on 27.02.2016.
 */
public class PauseScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor helpButton;
    private ImageActor settingsButton;
    private ImageActor exitButton;

    public PauseScreen (SpriteBatch batch) {
        backGround = new ImageActor(new Texture("android/assets/Pause_menu.png"), 0, 0);
        playButton = new ImageActor(new Texture("android/assets/resume_btn.png"), 230, 0);
        playButton.addListener(new MoveToGamePlay());
        settingsButton = new ImageActor(new Texture("android/assets/setting_btn.png"), 190, 325);
        settingsButton.addListener(new MoveToSettingPause());
        helpButton = new ImageActor(new Texture("android/assets/Help_btn.png"), 190, 250);
        helpButton.addListener(new MoveToHelpPause());
        exitButton = new ImageActor(new Texture("android/assets/Exit_btn.png"), 190, 175);
        exitButton.addListener(new MoveToGame());


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
        stage.addActor(helpButton);
        stage.addActor(exitButton);

    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
