package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.MoveToGame;
import com.ads.yetigame.Controller.MoveToHelpMenu;
import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by rusme_000 on 06.02.2016.
 */
public class HelpScreen2 implements Screen {
    private Stage stage;
    private ImageActor screenBackGround;
    private ImageActor backButton;
    private ImageActor previousButton;

    public HelpScreen2 (SpriteBatch batch){
        screenBackGround = new ImageActor(new Texture("android/assets/Help_menu_2.png"),0,0);
        backButton = new ImageActor(new Texture("android/assets/back_btn.png"),0,0);
        backButton.addListener(new MoveToGame());
        previousButton = new ImageActor(new Texture("android/assets/previous_btn.png"),0,220);
        previousButton.addListener(new MoveToHelpMenu());

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(screenBackGround);
        stage.addActor(backButton);
        stage.addActor(previousButton);
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}