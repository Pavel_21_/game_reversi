package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.MoveToGamePlay;
import com.ads.yetigame.Controller.MoveToPauseScreen;
import com.ads.yetigame.Model.ButtonState;
import com.ads.yetigame.Model.Field;
import com.ads.yetigame.View.Button;
import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.YetiGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by rusme_000 on 27.02.2016.
 */
public class GameScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor pauseButton;
    private Button[][] chips;
    private ImageActor againButton;
    private Field field;
    private int red;
    private int black;
    private int sumChips;
    private ImageActor redWon;
    private ImageActor blackWon;
    private ImageActor tie;


    public GameScreen(final SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        backGround = new ImageActor(textureRegions.get("Game Background"), 0,0);
        pauseButton = new ImageActor(new Texture("android/assets/pause_btn.png"), 0, 380);
        pauseButton.addListener(new MoveToPauseScreen());
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage() {
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                int x = (screenX - 86) / 39;
                int y = (Gdx.graphics.getHeight() - screenY - 81) / 39;
                field.placeAt(x, y);
                updateChips();
                return false;
            }
        };
        stage.addActor(backGround);
        stage.addActor(pauseButton);
        chips = new Button[10][10];
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                chips[i][j] = new Button(textureRegions.get("BlackChip"), textureRegions.get("RedChip"), i * 39 + 86, j * 39 + 81);
                stage.addActor(chips[i][j]);
            }
        field = new Field();
        updateChips();
        
    }
    public void updateChips() {
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                switch (field.getCell(i, j)) {
                    case 0:
                        chips[i][j].setState(ButtonState.NONE);
                        break;
                    case 1:
                        chips[i][j].setState(ButtonState.BLACK);
                        break;
                    case 2:
                        chips[i][j].setState(ButtonState.RED);
                        break;
                }
            }
        countChips();
        if(sumChips==64) {
            gameOver();
            refreshField();
        }

    }
    public void gameOver(){
        red = 0;
        black = 0;
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                switch (field.getCell(i, j)) {
                    case 0:
                        chips[i][j].setState(ButtonState.NONE);
                        break;
                    case 1:
                        chips[i][j].setState(ButtonState.BLACK);
                        black++;
                        break;
                    case 2:
                        chips[i][j].setState(ButtonState.RED);
                        red++;
                        break;
                }
            }
        if(red>black){
            redWon = new ImageActor(new Texture("android/assets/Red_won_.png"), 240, 280);
            stage.addActor(redWon);
        }if (black>red){
            blackWon = new ImageActor(new Texture("android/assets/Black_won_.png"), 240, 280);
            stage.addActor(blackWon);
        }
        if(black == red){
            tie = new ImageActor(new Texture("android/assets/Tie_.png"), 265, 320);
            stage.addActor(tie);
        }
        againButton = new ImageActor(new Texture("android/assets/resume_btn.png"), 200, 90);
        againButton.addListener(new ClickListener(){
            public void clicked(InputEvent event,float x,float y){
                YetiGame.getInstance().moveToGameMenu();
                YetiGame.getInstance().resetGameScreen();
                YetiGame.getInstance().moveToGame();
            }
        });
        stage.addActor(againButton);


    }
    private int countChips(){
        red = 0;
        black = 0;
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                switch (field.getCell(i, j)) {
                    case 0:
                        chips[i][j].setState(ButtonState.NONE);
                        break;
                    case 1:
                        chips[i][j].setState(ButtonState.BLACK);
                        black++;
                        break;
                    case 2:
                        chips[i][j].setState(ButtonState.RED);
                        red++;
                        break;
                }
            }
        sumChips = red+black;
        return sumChips;
    }
    private void refreshField(){
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                chips[i][j].setState(ButtonState.NONE);
            }
        chips[4][5].setState(ButtonState.BLACK);
        chips[4][4].setState(ButtonState.RED);
        chips[5][5].setState(ButtonState.RED);
        chips[5][4].setState(ButtonState.BLACK);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}
