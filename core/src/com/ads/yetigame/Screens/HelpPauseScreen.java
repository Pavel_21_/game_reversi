package com.ads.yetigame.Screens;

import com.ads.yetigame.Controller.MoveToHelpPause2;
import com.ads.yetigame.Controller.MoveToPauseScreen;
import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

/**
 * Created by rusme_000 on 27.02.2016.
 */
public class HelpPauseScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor nextButton;
    private ImageActor backButton;

    public HelpPauseScreen(SpriteBatch batch) {
        backGround = new ImageActor(new Texture("android/assets/Help_menu_1.png"), 0, 0);
        backButton = new ImageActor(new Texture("android/assets/back_btn.png"), 0, 0);
        backButton.addListener(new MoveToPauseScreen());
        nextButton = new ImageActor(new Texture("android/assets/next_btn.png"), 600, 220);
        nextButton.addListener(new MoveToHelpPause2());


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(backButton);
        stage.addActor(nextButton);
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}


